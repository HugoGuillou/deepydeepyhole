// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DeepyDeepyHole/Public/DAS_Game.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDAS_Game() {}
// Cross Module References
	DEEPYDEEPYHOLE_API UScriptStruct* Z_Construct_UScriptStruct_FGameInfo();
	UPackage* Z_Construct_UPackage__Script_DeepyDeepyHole();
	DEEPYDEEPYHOLE_API UClass* Z_Construct_UClass_UDAS_Game_NoRegister();
	DEEPYDEEPYHOLE_API UClass* Z_Construct_UClass_UDAS_Game();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
// End Cross Module References
class UScriptStruct* FGameInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DEEPYDEEPYHOLE_API uint32 Get_Z_Construct_UScriptStruct_FGameInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGameInfo, Z_Construct_UPackage__Script_DeepyDeepyHole(), TEXT("GameInfo"), sizeof(FGameInfo), Get_Z_Construct_UScriptStruct_FGameInfo_Hash());
	}
	return Singleton;
}
template<> DEEPYDEEPYHOLE_API UScriptStruct* StaticStruct<FGameInfo>()
{
	return FGameInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGameInfo(FGameInfo::StaticStruct, TEXT("/Script/DeepyDeepyHole"), TEXT("GameInfo"), false, nullptr, nullptr);
static struct FScriptStruct_DeepyDeepyHole_StaticRegisterNativesFGameInfo
{
	FScriptStruct_DeepyDeepyHole_StaticRegisterNativesFGameInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("GameInfo")),new UScriptStruct::TCppStructOps<FGameInfo>);
	}
} ScriptStruct_DeepyDeepyHole_StaticRegisterNativesFGameInfo;
	struct Z_Construct_UScriptStruct_FGameInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Public/DAS_Game.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGameInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGameInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameInfo_Statics::NewProp_PlayerSpeed_MetaData[] = {
		{ "Category", "GameInfo" },
		{ "ModuleRelativePath", "Public/DAS_Game.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FGameInfo_Statics::NewProp_PlayerSpeed = { "PlayerSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameInfo, PlayerSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FGameInfo_Statics::NewProp_PlayerSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameInfo_Statics::NewProp_PlayerSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGameInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameInfo_Statics::NewProp_PlayerSpeed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGameInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DeepyDeepyHole,
		nullptr,
		&NewStructOps,
		"GameInfo",
		sizeof(FGameInfo),
		alignof(FGameInfo),
		Z_Construct_UScriptStruct_FGameInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGameInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGameInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGameInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DeepyDeepyHole();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GameInfo"), sizeof(FGameInfo), Get_Z_Construct_UScriptStruct_FGameInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGameInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGameInfo_Hash() { return 3531858273U; }
	void UDAS_Game::StaticRegisterNativesUDAS_Game()
	{
	}
	UClass* Z_Construct_UClass_UDAS_Game_NoRegister()
	{
		return UDAS_Game::StaticClass();
	}
	struct Z_Construct_UClass_UDAS_Game_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_items_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_items;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_items_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDAS_Game_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_DeepyDeepyHole,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDAS_Game_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DAS_Game.h" },
		{ "ModuleRelativePath", "Public/DAS_Game.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDAS_Game_Statics::NewProp_items_MetaData[] = {
		{ "Category", "DAS_Game" },
		{ "ModuleRelativePath", "Public/DAS_Game.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDAS_Game_Statics::NewProp_items = { "items", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDAS_Game, items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDAS_Game_Statics::NewProp_items_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_Game_Statics::NewProp_items_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDAS_Game_Statics::NewProp_items_Inner = { "items", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGameInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDAS_Game_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDAS_Game_Statics::NewProp_items,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDAS_Game_Statics::NewProp_items_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDAS_Game_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDAS_Game>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDAS_Game_Statics::ClassParams = {
		&UDAS_Game::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDAS_Game_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_Game_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDAS_Game_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_Game_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDAS_Game()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDAS_Game_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDAS_Game, 819269295);
	template<> DEEPYDEEPYHOLE_API UClass* StaticClass<UDAS_Game>()
	{
		return UDAS_Game::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDAS_Game(Z_Construct_UClass_UDAS_Game, &UDAS_Game::StaticClass, TEXT("/Script/DeepyDeepyHole"), TEXT("UDAS_Game"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDAS_Game);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
