// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEEPYDEEPYHOLE_DAS_GameParameters_generated_h
#error "DAS_GameParameters.generated.h already included, missing '#pragma once' in DAS_GameParameters.h"
#endif
#define DEEPYDEEPYHOLE_DAS_GameParameters_generated_h

#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FItemInfo_Statics; \
	DEEPYDEEPYHOLE_API static class UScriptStruct* StaticStruct();


template<> DEEPYDEEPYHOLE_API UScriptStruct* StaticStruct<struct FItemInfo>();

#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_SPARSE_DATA
#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_RPC_WRAPPERS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDAS_GameParameters(); \
	friend struct Z_Construct_UClass_UDAS_GameParameters_Statics; \
public: \
	DECLARE_CLASS(UDAS_GameParameters, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UDAS_GameParameters)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUDAS_GameParameters(); \
	friend struct Z_Construct_UClass_UDAS_GameParameters_Statics; \
public: \
	DECLARE_CLASS(UDAS_GameParameters, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UDAS_GameParameters)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDAS_GameParameters(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDAS_GameParameters) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDAS_GameParameters); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDAS_GameParameters); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDAS_GameParameters(UDAS_GameParameters&&); \
	NO_API UDAS_GameParameters(const UDAS_GameParameters&); \
public:


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDAS_GameParameters(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDAS_GameParameters(UDAS_GameParameters&&); \
	NO_API UDAS_GameParameters(const UDAS_GameParameters&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDAS_GameParameters); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDAS_GameParameters); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDAS_GameParameters)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PlayerName() { return STRUCT_OFFSET(UDAS_GameParameters, PlayerName); } \
	FORCEINLINE static uint32 __PPO__PlayerSpeed() { return STRUCT_OFFSET(UDAS_GameParameters, PlayerSpeed); }


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_21_PROLOG
#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_RPC_WRAPPERS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_INCLASS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_INCLASS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEEPYDEEPYHOLE_API UClass* StaticClass<class UDAS_GameParameters>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DeepyDeepyHole_Source_DeepyDeepyHole_Private_DAS_GameParameters_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
