// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DeepyDeepyHole/Private/DAS_GameParameters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDAS_GameParameters() {}
// Cross Module References
	DEEPYDEEPYHOLE_API UScriptStruct* Z_Construct_UScriptStruct_FItemInfo();
	UPackage* Z_Construct_UPackage__Script_DeepyDeepyHole();
	DEEPYDEEPYHOLE_API UClass* Z_Construct_UClass_UDAS_GameParameters_NoRegister();
	DEEPYDEEPYHOLE_API UClass* Z_Construct_UClass_UDAS_GameParameters();
	ENGINE_API UClass* Z_Construct_UClass_UPrimaryDataAsset();
// End Cross Module References
class UScriptStruct* FItemInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DEEPYDEEPYHOLE_API uint32 Get_Z_Construct_UScriptStruct_FItemInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FItemInfo, Z_Construct_UPackage__Script_DeepyDeepyHole(), TEXT("ItemInfo"), sizeof(FItemInfo), Get_Z_Construct_UScriptStruct_FItemInfo_Hash());
	}
	return Singleton;
}
template<> DEEPYDEEPYHOLE_API UScriptStruct* StaticStruct<FItemInfo>()
{
	return FItemInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FItemInfo(FItemInfo::StaticStruct, TEXT("/Script/DeepyDeepyHole"), TEXT("ItemInfo"), false, nullptr, nullptr);
static struct FScriptStruct_DeepyDeepyHole_StaticRegisterNativesFItemInfo
{
	FScriptStruct_DeepyDeepyHole_StaticRegisterNativesFItemInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("ItemInfo")),new UScriptStruct::TCppStructOps<FItemInfo>);
	}
} ScriptStruct_DeepyDeepyHole_StaticRegisterNativesFItemInfo;
	struct Z_Construct_UScriptStruct_FItemInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FItemInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Private/DAS_GameParameters.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FItemInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FItemInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FItemInfo_Statics::NewProp_PlayerSpeed_MetaData[] = {
		{ "Category", "ItemInfo" },
		{ "ModuleRelativePath", "Private/DAS_GameParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FItemInfo_Statics::NewProp_PlayerSpeed = { "PlayerSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FItemInfo, PlayerSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FItemInfo_Statics::NewProp_PlayerSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FItemInfo_Statics::NewProp_PlayerSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FItemInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FItemInfo_Statics::NewProp_PlayerSpeed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FItemInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DeepyDeepyHole,
		nullptr,
		&NewStructOps,
		"ItemInfo",
		sizeof(FItemInfo),
		alignof(FItemInfo),
		Z_Construct_UScriptStruct_FItemInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FItemInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FItemInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FItemInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FItemInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FItemInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DeepyDeepyHole();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ItemInfo"), sizeof(FItemInfo), Get_Z_Construct_UScriptStruct_FItemInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FItemInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FItemInfo_Hash() { return 190675117U; }
	void UDAS_GameParameters::StaticRegisterNativesUDAS_GameParameters()
	{
	}
	UClass* Z_Construct_UClass_UDAS_GameParameters_NoRegister()
	{
		return UDAS_GameParameters::StaticClass();
	}
	struct Z_Construct_UClass_UDAS_GameParameters_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlayerSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlayerName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDAS_GameParameters_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimaryDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_DeepyDeepyHole,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDAS_GameParameters_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DAS_GameParameters.h" },
		{ "ModuleRelativePath", "Private/DAS_GameParameters.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerSpeed_MetaData[] = {
		{ "Category", "DAS_GameParameters" },
		{ "ModuleRelativePath", "Private/DAS_GameParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerSpeed = { "PlayerSpeed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDAS_GameParameters, PlayerSpeed), METADATA_PARAMS(Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerName_MetaData[] = {
		{ "Category", "DAS_GameParameters" },
		{ "ModuleRelativePath", "Private/DAS_GameParameters.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerName = { "PlayerName", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDAS_GameParameters, PlayerName), METADATA_PARAMS(Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDAS_GameParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDAS_GameParameters_Statics::NewProp_PlayerName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDAS_GameParameters_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDAS_GameParameters>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDAS_GameParameters_Statics::ClassParams = {
		&UDAS_GameParameters::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDAS_GameParameters_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_GameParameters_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDAS_GameParameters_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDAS_GameParameters_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDAS_GameParameters()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDAS_GameParameters_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDAS_GameParameters, 3254847442);
	template<> DEEPYDEEPYHOLE_API UClass* StaticClass<UDAS_GameParameters>()
	{
		return UDAS_GameParameters::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDAS_GameParameters(Z_Construct_UClass_UDAS_GameParameters, &UDAS_GameParameters::StaticClass, TEXT("/Script/DeepyDeepyHole"), TEXT("UDAS_GameParameters"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDAS_GameParameters);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
