// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEEPYDEEPYHOLE_MyDataAsset_generated_h
#error "MyDataAsset.generated.h already included, missing '#pragma once' in MyDataAsset.h"
#endif
#define DEEPYDEEPYHOLE_MyDataAsset_generated_h

#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_SPARSE_DATA
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_RPC_WRAPPERS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyDataAsset(); \
	friend struct Z_Construct_UClass_UMyDataAsset_Statics; \
public: \
	DECLARE_CLASS(UMyDataAsset, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UMyDataAsset)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyDataAsset(); \
	friend struct Z_Construct_UClass_UMyDataAsset_Statics; \
public: \
	DECLARE_CLASS(UMyDataAsset, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UMyDataAsset)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyDataAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyDataAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyDataAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyDataAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyDataAsset(UMyDataAsset&&); \
	NO_API UMyDataAsset(const UMyDataAsset&); \
public:


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyDataAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyDataAsset(UMyDataAsset&&); \
	NO_API UMyDataAsset(const UMyDataAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyDataAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyDataAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyDataAsset)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_PRIVATE_PROPERTY_OFFSET
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_12_PROLOG
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_RPC_WRAPPERS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_INCLASS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_INCLASS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEEPYDEEPYHOLE_API UClass* StaticClass<class UMyDataAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DeepyDeepyHole_Source_DeepyDeepyHole_Public_MyDataAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
