// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEEPYDEEPYHOLE_DAS_Game_generated_h
#error "DAS_Game.generated.h already included, missing '#pragma once' in DAS_Game.h"
#endif
#define DEEPYDEEPYHOLE_DAS_Game_generated_h

#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGameInfo_Statics; \
	DEEPYDEEPYHOLE_API static class UScriptStruct* StaticStruct();


template<> DEEPYDEEPYHOLE_API UScriptStruct* StaticStruct<struct FGameInfo>();

#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_SPARSE_DATA
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_RPC_WRAPPERS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDAS_Game(); \
	friend struct Z_Construct_UClass_UDAS_Game_Statics; \
public: \
	DECLARE_CLASS(UDAS_Game, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UDAS_Game)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUDAS_Game(); \
	friend struct Z_Construct_UClass_UDAS_Game_Statics; \
public: \
	DECLARE_CLASS(UDAS_Game, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DeepyDeepyHole"), NO_API) \
	DECLARE_SERIALIZER(UDAS_Game)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDAS_Game(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDAS_Game) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDAS_Game); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDAS_Game); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDAS_Game(UDAS_Game&&); \
	NO_API UDAS_Game(const UDAS_Game&); \
public:


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDAS_Game(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDAS_Game(UDAS_Game&&); \
	NO_API UDAS_Game(const UDAS_Game&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDAS_Game); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDAS_Game); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDAS_Game)


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__items() { return STRUCT_OFFSET(UDAS_Game, items); }


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_22_PROLOG
#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_RPC_WRAPPERS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_INCLASS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_PRIVATE_PROPERTY_OFFSET \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_SPARSE_DATA \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_INCLASS_NO_PURE_DECLS \
	DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEEPYDEEPYHOLE_API UClass* StaticClass<class UDAS_Game>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DeepyDeepyHole_Source_DeepyDeepyHole_Public_DAS_Game_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
