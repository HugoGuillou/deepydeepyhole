// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DAS_Game.generated.h"

/**
 * 
 */

USTRUCT()
struct FGameInfo {

    GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere)
        FString PlayerSpeed;
};

UCLASS()
class DEEPYDEEPYHOLE_API UDAS_Game : public UDataAsset
{
	GENERATED_BODY()
	
        UPROPERTY(EditAnywhere)
        TArray<FGameInfo> items;
};
