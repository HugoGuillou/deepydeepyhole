// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DAS_GameParameters.generated.h"

/**
 * 
 */
USTRUCT()
struct FItemInfo {

    GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere)
        FString PlayerSpeed;
};

UCLASS()
class UDAS_GameParameters : public UPrimaryDataAsset
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    FString PlayerName; 

    UPROPERTY(EditAnywhere)
    float PlayerSpeed;
	
};